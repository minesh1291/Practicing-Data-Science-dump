
For TensorBoard

* Open with Terminal
```bash 
tensorboard --logdir=./logs 
```

* Run training with jupyter notebook
  * previous cell
  ```bash
  !rm -rf ./logs/*
  ```
  * train cell
  ```python
  tb = TensorBoard()
  model.fit(...,callbacks=[tb])
  ```
check the notebook [try0.ipynb](https://github.com/minesh1291/Practicing-Data-Science-dump/blob/master/TLV_DL/try0.ipynb) for custom visialization

* Visualize the updates during training

![TB](https://github.com/minesh1291/Practicing-Data-Science-dump/raw/master/TLV_DL/TensorBoard_Try0.png)
