```
gpu:
  problem:
    nvidia-smi VML: Driver/library version mismatch
  solution:
    sudo apt-get purge nvidia* (good command)
    sudo apt-get update
    sudo apt-get install nvidia-390
    Reboot your computer for the new driver to kick-in.
    sudo apt-mark hold nvidia-390
  problem:
    NVIDIA-SMI has failed because it couldn't communicate with the NVIDIA driver
  solution:
    sudo apt-get install cuda (good command)
    sudo apt-get install cuda-drivers
    **alt**
      # The 16.04 installer works with 16.10.
      curl -O http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/cuda-repo-ubuntu1604_8.0.61-1_amd64.deb
      dpkg -i ./cuda-repo-ubuntu1604_8.0.61-1_amd64.deb
      apt-get update
      apt-get install cuda -y
    restart computer
    libcnn if not already installed
    testrun >> run the notebook present in same folder
      
```
