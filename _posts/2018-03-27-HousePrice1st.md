---
layout: post
title: Let's start with well known dataset
---

27 March 2018

We will start to build simple linear regression model for Predicting house pricing.
Description about the House Pricing Data, you can find on Kaggle Competition page
You can try running simple linear regression model using only 2 features on kaggle kernel
This is a very simple model to begin, in future we will perform data cleaning, exploratory analysis and advanced modelling techniques, etc.
But at this level you can try fork and run the given kernel, main goal here is to introduce you to simple python script for modelling using pandas, numpy and scikit learn.
To improve model first you can note down current RMSE for model, then you can try using other feature than which we already used and have hands-on experiance.
If you encounter any problem you can create issue with script sniplet and output.
