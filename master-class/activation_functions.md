# ReLu {0,inf} (fast) > Tanh {-inf,inf} (accurate) > Sigmoid {0-1}

Most of time tanh is quickly converge than sigmoid and logistic function, and performs better accuracy [1]. However, recently rectified linear unit (ReLU) is proposed by Hinton [2] which shows ReLU train six times fast than tanh [3] to reach same training error. And you can refer to [4] to see what benefits ReLU provides.

Accordining to about 2 years machine learning experience. I want to share some stratrgies the most paper used and my experience about computer vision.

* Normalizing input is very important

Normalizing well could get better performance and converge quickly. Most of time we will subtract mean value to make input mean to be zero to prevent weights change same directions so that converge slowly [5] .Recently google also points that phenomenon as internal covariate shift out when training deep learning, and they proposed batch normalization [6] so as to normalize each vector having zero mean and unit variance.

* More data more accuracy

More training data could generize feature space well and prevent overfitting. In computer vision if training data is not enough, most of used skill to increase training dataset is data argumentation and synthesis training data.

* Choosing a good activation function allows training better and efficiently.

ReLU nonlinear acitivation worked better and performed state-of-art results in deep learning and MLP. Moreover, it has some benefits e.g. simple to implementation and cheaper computation in back-propagation to efficiently train more deep neural net. However, ReLU will get zero gradient and do not train when the unit is zero active. Hence some modified ReLUs are proposed e.g. Leaky ReLU, and Noise ReLU, and most popular method is PReLU [7] proposed by Microsoft which generalized the traditional recitifed unit.

* Others

choose large initial learning rate if it will not oscillate or diverge so as to find a better global minimum.
shuffling data

1. [link](http://www.cscjournals.org/manuscript/Journals/IJAE/volume1/Issue4/IJAE-26.pdf)
2. [link](http://machinelearning.wustl.edu/mlpapers/paper_files/icml2010_NairH10.pdf)
3. [link](http://www.cs.toronto.edu/~fritz/absps/imagenet.pdf)
4. [link](http://en.wikipedia.org/wiki/Rectifier_%28neural_networks%29)
5. [link](http://yann.lecun.com/exdb/publis/pdf/lecun-98b.pdf)
6. [link](http://arxiv.org/pdf/1502.03167v3.pdf)
7. [link](http://arxiv.org/pdf/1502.01852v1.pdf)
