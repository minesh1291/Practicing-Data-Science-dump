# 1. Load Data

from keras.layers.convolutional import Convolution2D, MaxPooling1D,Convolution1D,MaxPooling2D
from keras.layers.normalization import BatchNormalization
from keras.layers.core import Dense, Activation, Dropout, Flatten
from keras.models import Sequential
from keras.optimizers import SGD,Adagrad
import numpy as np
import pandas as pd

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

### Only things to change

dataFile = "./xy_train.csv"
queFile = "./x_test.csv"

#~ dataFile = "./master_train_100.csv"
#~ queFile = "./master_test_100.csv"

###






print ("^^^INFO: Fix random seed^^^")

seed = 7
np.random.seed(seed)

print ("^^^INFO: Load dataset^^^")
# dataset = numpy.loadtxt("diabetic_data_1000V2.csv", delimiter=",",comments="#")
dataset = np.genfromtxt(dataFile, delimiter=",",comments="#")

# mask = np.any(np.isnan(dataset), axis=0)
# dataset = dataset[:,~mask]

print ("^^^INFO: Shape of dataset^^^")
print (dataset.shape)

# split into input (X) and output (Y) variables
X = dataset[:, 0:dataset.shape[1]-1]
Y = dataset[:, dataset.shape[1]-1]

print ("^^^INFO: Shape of X^^^")
print (X.shape)
print ("^^^INFO: Shape of Y^^^")
print (Y.shape)

from keras.utils.np_utils import to_categorical
X = X.reshape(dataset.shape[0],2,113,1)
Y = to_categorical(Y)

print ("^^^INFO: Shape of X^^^")
print (X.shape)
print ("^^^INFO: Shape of Y^^^")
print (Y.shape)


print ("^^^INFO: Define Model^^^")

# create model
model = Sequential()


model.add(Convolution2D(16, 2, 6, border_mode='valid', input_shape=(2,113, 1)))
model.add(Activation('relu'))
# model.add(Dropout(0.25))

model.add(BatchNormalization())

model.add(Convolution2D(12, 1,3 , border_mode='valid'))
model.add(Activation('sigmoid'))
model.add(BatchNormalization())
# model.add(Dropout(0.25))
# model.add(Convolution1D(8, 6, border_mode='valid'))
# model.add(Activation('relu'))

model.add(MaxPooling2D(pool_size=(1,6)))
model.add(BatchNormalization())
model.add(Dropout(0.25))

model.add(Flatten())

model.add(Dense(70, init='uniform', activation='sigmoid'))
model.add(Dropout(0.25))

#~ model.add(Dense(100, init='uniform', activation='relu'))
#~ model.add(Dropout(0.5))
model.add(Dense(12, init='uniform', activation='relu'))
model.add(Dropout(0.25))
model.add(Dense(3, init='uniform', activation='softmax'))


print ("^^^INFO: Compile Model^^^")

#~ sgd = SGD(lr=0.01, momentum=0, decay=1e-4, nesterov=False)
adagrad=Adagrad(lr=0.01)
# Compile model
model.compile(loss='categorical_crossentropy', optimizer= adagrad, metrics=['accuracy'])

# categorical_crossentropy
# binary_crossentropy


print ("^^^INFO: Fit Model^^^")
history=model.fit(X, Y, nb_epoch=50, batch_size=30, validation_split=0.3)


print ("^^^INFO: Evaluate Model^^^")
scores = model.evaluate(X, Y)
print("\n%s: %.2f%%" % (model.metrics_names[1], scores[1] * 100))

print ("^^^INFO: Making plots^^^")
# list all data in history
print(history.history.keys())
# summarize history for accuracy
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
# axes = plt.gca()
# axes.set_xlim([0,120])
# axes.set_ylim([90,100])
plt.savefig('acc.png')   # save the figure to file
# plt.show()
plt.close()

# summarize history for loss
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.savefig('loss.png')
# plt.show()

#~ '''
print ("^^^INFO: Load dataset for Prediction^^^")
queset = np.genfromtxt(queFile, delimiter=",",comments="#")
pred_X = queset[:,:]
sampleFile = "../input/sample_submission.csv"
queset_id=pd.read_csv(sampleFile, sep=',',header=0)
print(queset_id.iloc[0:3,0])
n = queset_id.iloc[:,0]

print ("^^^INFO: Making Prediction^^^")
pred_X = pred_X.reshape(pred_X.shape[0],2, 113,1)
pred_Y = model.predict_proba(pred_X)

print ("^^^INFO: Add Label Prediction^^^")
names=['high','medium','low']
result_df = pd.DataFrame(pred_Y, index=n,columns=names)
result_df.index.name='listing_id'
print ("^^^INFO: Prediction to result_df.csv^^^")
result_df.to_csv('result_df.csv', index=True,header=True, sep=',',float_format="%.4f")

#~ '''
